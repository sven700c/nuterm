## nuterm for PAL and NTSC (15kHz)

This is a fork of luckynate4's nuterm firmware for the uTerm (and uTerm-S) boards.
What started out as tinkering to get different fonts compiled into the firmware,
quickly evolved into some heavy modifications to get a stable picture displayed
on my Sony PVM monitor via the 15kHz RBG inputs.

#### Main changes and added features:
 * Timings for PAL and NTSC via clock dividers and multipliers allowing to keep 
 the 25MHz crystal of the uTerm board in place.
 * Set background color on pin PB1 to enabled (can be jumpered to JP2 or JP3 
 for different color combinations)
 * Compiled in a second font and added a toggle in the menu (Ctrl-Brk then R)
 * PAL fonts compiled from YES-T & MCR and NTSC fonts compiled from 
 NEWFONT1 & DATA from [int10h font repo](https://github.com/viler-int10h/vga-text-mode-fonts)
 * Default Sync signal set to Composite Sync on pin PA6
 * Extensively modified fontedit python script [(see separate readme)](./font-tools/readme.md)
 * Edited the Makefile to use the CLI flash tool from STM32 Cube Programmer

#### Usage

Precompiled binaries are in the repo. You can flash them with:\
`make flash MYBIN=<nuterm*.bin>`\
or compile from source with `make` and/or `make flash` to flash the newly 
compiled binary.

#### Configuration Options

Defaults are set for PAL timings with two 10px high font sets compiled in
resulting in a 80x24 character display.

 * Toggle between PAL and NTSC in file [15khz-min.c](./src/15khz-min.h)
 * For NTSC change font height in file [vgafont.h](./src/vgafont.h) to 9px
 and copy scanlines.fnt.NTSC1.c and *.NTSC2.c to scanlines.fnt.c and 
 scanlines.fnt.alt.c respectively
 * If you want only one font set, comment out line `#define DUALFONT` in 
 [vgafont.h](./src/vgafont.h) (necessary if font is higher than 10px)

#### References

- [STM32F0x0 Documentation][stm32f0x0-doc]. Note that this is a different
  documentation page from the STM32F0x[128] series.
- [RM0360 Reference manual: STM32F030x4/x6/x8/xC and STM32F070x6/xB advanced
  ARM® -based 32-bit MCUs][stm32f0x0-RM0360]

[stm32f0x0-RM0360]: https://www.st.com/resource/en/reference_manual/rm0360-stm32f030x4x6x8xc-and-stm32f070x6xb-advanced-armbased-32bit-mcus-stmicroelectronics.pdf
[stm32f0x0-doc]: https://www.st.com/en/microcontrollers-microprocessors/stm32f0x0-value-line.html#documentation


\
original README by luckynate4 below:

---


## nuterm 

nuterm is a modified firmware for the uTerm (and uTerm-S) boards.
The uTerm boards are a great way to build a hardware terminal
with VGA output and a PS/2 keyboard input that you can connect to your
serial devices.

What is different about nuterm?
 * ported from Keil uVision compiler to GCC.
 * added a bugfix from https://github.com/gdampf/uTerm - sends keyboard reset code
 * fixed several keyboard logic bugs regarding SHIFT and NUMLOCK handling
 * added ability to send ALT- key combos (like ALT-x in EMACS)
 * added a menu mode that allows you to switch baud rates and data settings on the fly.
 * added ability to change backspace from Ctrl-H to Ctrl-? (in menu mode, or by default)
 * expanded the VT100/VT52 emulation to make it perform better with *nix programs, and Thomas Dickey's vttest.
 * changed font to IBM VGA 8x16 from the Ultimate Oldschool PC fontpack (https://int10h.org/).


Why?
 * I am not a Windows guy, so I wanted to port to Linux/open source tools.
 * As a learning experience.
 * To add some extra features to the firmware, and fix some bugs.

### Background 

uTerm and uTerm-S are circuit boards designed by Just4Fun that provide a terminal 
with VGA output and PS/2 keyboard input to a serial device.  The plain uTerm
connects with TTL level serial devices, and the uTerm-S has an RS-232 connector.

https://hackaday.io/project/165325 
https://github.com/SuperFabius/uTerm

uTerm was derived from the work of K. C. Lee who designed the
original ChibiTerm terminal, and firmware. 

https://github.com/FPGA-Computer/STM32F030F4-VGA 
https://hw-by-design.blogspot.com/2018/07/low-cost-vga-terminal-module-project.html

Madis Kaal modified the ChibiTerm firmware to include 
VT100 emulation, font editor, 16-line font, and support for inverse fonts

http://www.nomad.ee/micros/chibiterm.shtml

### Precompiled binaries

You can find several precompiled binaries ready for flashing:
 * nuterm-menu.bin (Menu Mode enabled)
 * nuterm-no_menu.bin (No menu mode, with a message showing starting baud rate)
 * nuterm-no_menu-no_startmsg.bin (self explanatory)

### Compiling

Note: I am developing this on Fedora 34 with arm-none-eabi-gcc-10.2.0

1. Edit the src/config.h file to choose appropriate options.
2. Run the ``make'' command in the root directory of this project.
3. Flash the resulting nuterm.bin file.  If flashing fails, try to reset the 
uTerm (with the reset button on the PCB), then flash again.  That works for me.


### License(s)

Source code distributed under GPLV3, with exception of fontedit.py that is
distributed under BSD license.
