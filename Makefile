# nuterm makefile

# Adjust the install dir and executable name of the CLI flash tool below:
ST_TOOL_DIR?=/Applications/STMicroelectronics/STM32Cube/STM32CubeProgrammer/STM32CubeProgrammer.app/Contents/MacOs/bin
ST_CLI_TOOL?=$(ST_TOOL_DIR)/STM32_Programmer_CLI

DEVICE = STM32F030

SRCDIR = src
OBJDIR = obj

CSRC += $(SRCDIR)/ansi.c \
	$(SRCDIR)/fifo.c \
	$(SRCDIR)/ps2.c \
	$(SRCDIR)/rcc.c \
	$(SRCDIR)/menu.c \
	$(SRCDIR)/serial.c \
	$(SRCDIR)/term.c \
	$(SRCDIR)/vga-min.c \
	$(SRCDIR)/vgafont.c 

ASMSRC = $(SRCDIR)/startup_stm32f030.s

_OBJ = $(CSRC:.c=.o) $(ASMSRC:.s=.o)
OBJ = $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(_OBJ))
DEP = $(OBJ:.o=.d)

BIN = $(OBJDIR)/nuterm

PREFIX  = arm-none-eabi-
CC = $(PREFIX)gcc
LD = $(PREFIX)ld
OBJCOPY = $(PREFIX)objcopy
OBJDUMP = $(PREFIX)objdump

INCLUDE =  -I ./include/ \
	   -I /usr/local/arm-none-eabi/include/
LIBS    =  -lgcc

CPPFLAGS = $(INCLUDE) -MMD 
CFLAGS   =  -Os -ffunction-sections -fdata-sections -g -Wall -Wimplicit-fallthrough \
            -D$(DEVICE) -mcpu=cortex-m0 -mthumb 

LINKER_SCRIPT = $(SRCDIR)/linker.ld
LDFLAGS = -T$(LINKER_SCRIPT) -nostartfiles -Wl,-Map=$(BIN).map,--cref,--gc-sections


all: $(BIN).elf $(BIN).bin $(BIN).lst

-include $(DEP)

$(BIN).elf: $(OBJ)
	@echo "Linking $@"
	@$(CC) $(OBJ) $(LIBS) $(LDFLAGS) $(CFLAGS)--output $@

$(BIN).bin: $(BIN).elf
	@echo "Building $@"
	@$(OBJCOPY) -O binary $(BIN).elf $(BIN).bin

$(BIN).lst: $(BIN).elf
	@echo "Building $@"
	@$(OBJDUMP) -D $(BIN).elf > $(BIN).lst

$(OBJDIR)/%.o: $(SRCDIR)/%.c
	@echo "Compiling $<"
	@$(CC) -c $(CPPFLAGS) $(CFLAGS) -o $@ $<

$(OBJDIR)/%.o: $(SRCDIR)/%.s
	@echo "Compiling $<"
	@$(CC) -c $(CPPFLAGS) $(CFLAGS) -o $@ $<

.PHONY: clean flash
clean:
	@echo "Cleaning ..."
	@rm -f $(OBJ)
	@rm -f $(DEP)
	@rm -f $(BIN).elf
	@rm -f $(BIN).bin
	@rm -f $(BIN).lst
	@rm -f $(BIN).map

# You can submit MYBIN=<path to compiled binary> to overwrite $(BIN)
flash: $(if $(MYBIN),$(MYBIN),$(BIN).bin)
	$(ST_CLI_TOOL) \
		--connect port=SWD \
		--write $(if $(MYBIN),$(MYBIN),$(BIN).bin) 0x08000000 \
		--verify \
		-rst
