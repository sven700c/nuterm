# The MIT License (MIT)
#
#  Copyright (c) 2017 Madis Kaal <mast@nomad.ee>
#
#  Permission is hereby granted, free of charge, to any person obtaining a copy
#  of this software and associated documentation files (the "Software"), to deal
#  in the Software without restriction, including without limitation the rights
#  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#  copies of the Software, and to permit persons to whom the Software is
#  furnished to do so, subject to the following conditions:
#
#  The above copyright notice and this permission notice shall be included in all
#  copies or substantial portions of the Software.
#
#  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
#  SOFTWARE.

import curses
import argparse
import os
import pandas as pd

screen = None
cell = []
cx = 0
cy = 0
clip = None
filename = None

for i in range(0, 16):
    cell.append(0)
currentchar = ord('A')
markchar = None


def setfontheight(r, x):
    global font, cell, cy
    fontrows = font.shape[1]

    if r == fontrows:
        return

    if r < fontrows:
        if x:
            font = font.iloc[:, -r:]
            cell = cell[-r:]
            cy = max(cy - (fontrows - r), 0)
        else:
            font = font.iloc[:, :r]
            cell = cell[:r]
            cy = 0
    else:
        new_cols = pd.DataFrame(0, index=font.index,
                                columns=range(r - fontrows))
        if x:
            font = pd.concat([new_cols, font], axis=1)
            cell = [0] * (r - fontrows) + cell
        else:
            font = pd.concat([font, new_cols], axis=1)
            cell = cell + [0] * (r - fontrows)

    clip = None
    markchar = None
    return


def load(file):
    global font, cell, currentchar, filename
    filename = file
    try:
        f = open(filename, "rb")
        data = f.read()
        f.close()
    except:
        return

    r = len(data) // 256
    if 0 < r < 21:
        font_data = [[bytearray([data[c * r + i]])[0]
                      for i in range(r)] for c in range(256)]
        font = pd.DataFrame(font_data)
        setfontheight(r, 0)
        cell = list(font.iloc[currentchar])
    return


def save():
    global cell, font, currentchar, filename
    font.iloc[currentchar] = list(cell)
    os.rename(filename, filename + "~")
    with open(filename, "wb") as f:
        for c in range(0, 256):
            for r in range(0, len(cell)):
                f.write(bytes([font.iat[c, r]]))
    return


def export():
    global cell, font, currentchar, filename
    font.iloc[currentchar] = list(cell)
    with open(filename + ".c", "wt") as f:
        f.write("  // %s\n" % filename)
        for c in range(0, 256):
            f.write("  // 0x%02x" % c)
            if 31 < c < 127:
                f.write(" '%c'" % chr(c))
            f.write("\n  ")
            for r in range(0, len(cell)):
                f.write("0x%02x," % font.iat[c, r])
            f.write("\n")
    return


def export_scanlines():
    global font, filename
    outfilename = "scanlines.fnt.c"
    with open(outfilename, "w") as f:
        f.write(f"// Generated from {filename}\n\n")
        for r in range(0, font.shape[1]):
            f.write(f"// scanline {r}\n")
            for c in range(0, 256):
                if r < len(font.iloc[c]):
                    f.write(f"0x{font.iat[c, r]:02x},")
                else:
                    f.write("00h ")
                if (c + 1) % 16 == 0:
                    f.write("\n")
            f.write("\n")
    return


def relocate():
    global font

    if font.shape[1] <= 14:
        ht = pd.Series([
            0x50, 0x50, 0x70, 0x5e, 0x54, 0x04, 0x04, 0x04])
        ff = pd.Series([
            0x70, 0x40, 0x60, 0x4e, 0x48, 0x0c, 0x08, 0x08])
        cr = pd.Series([
            0x30, 0x40, 0x40, 0x4e, 0x3a, 0x0c, 0x0a, 0x0a])
        lf = pd.Series([
            0x40, 0x40, 0x40, 0x4e, 0x78, 0x0c, 0x08, 0x08])
        nl = pd.Series([
            0x90, 0xd0, 0xb0, 0x98, 0x98, 0x08, 0x08, 0x0e])
        vt = pd.Series([
            0x50, 0x50, 0x50, 0x5e, 0x24, 0x04, 0x04, 0x04])
        hi_bar = pd.Series([
            0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
        mhi_bar = pd.Series([
            0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00])
        mlo_bar = pd.Series([
            0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00])
        lo_bar = pd.Series([
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00])
        n_eq = pd.Series([
            0x00, 0x00, 0x04, 0x7e, 0x18, 0x7e, 0x20, 0x00])
    else:
        ht = pd.Series([
            0x00, 0x88, 0x88, 0x88, 0xf8, 0x88, 0x88, 0x88,
            0x00, 0x3e, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08])
        ff = pd.Series([
            0x00, 0xf8, 0x80, 0x80, 0xf0, 0x80, 0x80, 0x80,
            0x3e, 0x20, 0x20, 0x3c, 0x20, 0x20, 0x20, 0x00])
        cr = pd.Series([
            0x00, 0x70, 0x88, 0x80, 0x80, 0x80, 0x88, 0x70,
            0x00, 0x3c, 0x22, 0x22, 0x3c, 0x28, 0x24, 0x22])
        lf = pd.Series([
            0x00, 0x80, 0x80, 0x80, 0x80, 0x80, 0x80, 0xf8,
            0x00, 0x3e, 0x20, 0x20, 0x3c, 0x20, 0x20, 0x20])
        nl = pd.Series([
            0x00, 0x84, 0xc4, 0xa4, 0xa4, 0x94, 0x94, 0x8c,
            0x84, 0x20, 0x20, 0x20, 0x20, 0x20, 0x20, 0x3e])
        vt = pd.Series([
            0x00, 0x88, 0x88, 0x88, 0x50, 0x50, 0x20, 0x20,
            0x00, 0x3e, 0x08, 0x08, 0x08, 0x08, 0x08, 0x08])
        hi_bar = pd.Series([
            0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
        mhi_bar = pd.Series([
            0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00])
        mlo_bar = pd.Series([
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0xff, 0x00, 0x00, 0x00, 0x00])
        lo_bar = pd.Series([
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00,
            0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xff, 0x00])
        n_eq = pd.Series([
            0x00, 0x00, 0x00, 0x00, 0x02, 0x04, 0x08, 0xfe,
            0x10, 0xfe, 0x20, 0x40, 0x80, 0x00, 0x00, 0x00])

    reloc_dict = {0: 4, 1: 176, 2: ht, 3: ff, 4: cr, 5: lf, 6: 248, 7: 241,
                  8: nl, 9: vt, 10: 217, 11: 191, 12: 218, 13: 192, 14: 197,
                  15: hi_bar, 16: mhi_bar, 17: 196, 18: mlo_bar, 19: lo_bar,
                  20: 195, 21: 180, 22: 193, 23: 194, 24: 179, 25: 243,
                  26: 242, 27: 227, 28: 156, 29: n_eq, 30: 250}
    """
    reloc_dict = {0:4, 1:176, 6:248, 7:241,
                10:217, 11:191, 12:218, 13:192, 14:197,
                17:196, 20:195, 21:180, 22:193, 23:194,
                24:179, 25:243, 26:242, 27:227, 28:156, 30:250}
    """
    for new_loc, old_loc in reloc_dict.items():
        if type(old_loc) == int:
            font.iloc[new_loc] = font.iloc[old_loc]
        else:
            if font.shape[1] == len(old_loc):
                font.iloc[new_loc] = old_loc
            else:
                append_pos = 1
                while font.shape[1] > len(old_loc):
                    if append_pos:
                        old_loc = pd.concat([old_loc, pd.Series([0])])
                        append_pos = 0
                    else:
                        old_loc = pd.concat([pd.Series([0]), old_loc])
                        append_pos = 1
                font.iloc[new_loc] = old_loc
    return


def invert():
    global font
    font.iloc[128:256] = font.iloc[:128]
    font.iloc[128:256] = font.iloc[128:256] ^ 0xFF
    return


def redraw():
    global screen, currentchar
    for y in range(0, len(cell)):
        v = cell[y]
        screen.addstr(y + 1, 0, str((y + 1) % 10))
        for c in range(0, 8):
            if v & 0x80:
                screen.addstr(y + 1, c * 2 + 2, '  ', curses.A_REVERSE)
            else:
                screen.addch(y + 1, c * 2 + 2, curses.ACS_BULLET)
                screen.addch(y + 1, c * 2 + 3, curses.ACS_BULLET)
            v = v << 1
    screen.addstr(1, 22, "0x%02x" % currentchar)
    if currentchar > 31 and currentchar != 127 and currentchar != 255:
        screen.addstr(3, 22, '"' + chr(currentchar) + '"')
    else:
        screen.addstr(3, 22, "   ")
    for i in range(len(cell), 21):
        screen.addstr(i + 1, 0, "                        ")
    screen.addstr(0, 2, "1 2 3 4 5 6 7 8")
    screen.move(cy + 1, cx * 2 + 2)
    screen.refresh()
    return


def help():
    global screen
    screen.addstr(2, 40, "q - save and quit")
    screen.addstr(3, 40, "Q - quit without saving")
    screen.addstr(4, 40, "arrow keys - move cursor")
    screen.addstr(5, 40, "pgup/pgdn - prev/next character")
    screen.addstr(6, 40, "space - flip dot")
    screen.addstr(7, 40, "i/I - invert character/block or upper 128 Chrs")
    screen.addstr(9, 40, "z - clear character")
    screen.addstr(10, 40, "r/R - rotate down/up")
    screen.addstr(11, 40, "")
    screen.addstr(12, 40, "T - make characters taller")
    screen.addstr(13, 40, "S - make characters shorter")
    screen.addstr(14, 40, "t - make characters taller from top")
    screen.addstr(15, 40, "s - make characters shorter from top")
    screen.addstr(16, 40, "")
    screen.addstr(17, 40, "m - mark block start")
    screen.addstr(18, 40, "c/C - copy character/block or chrs from list")
    screen.addstr(19, 40, "p - paste character/block")
    screen.addstr(20, 40, "")
    screen.addstr(21, 40, "E - export C source")
    screen.addstr(22, 40, "X - export as scanlines")


def status(s):
    global screen
    screen.move(0, 40)
    screen.clrtoeol()
    screen.addstr(0, 40, s)


def editor(stdscr):
    global screen, cx, cy, currentchar, cell, clip, markchar
    screen = stdscr
    screen.clear()
    help()
    while 1:
        redraw()
        c = screen.getch()
        status("")

        if c == ord('q'):
            save()
            break

        if c == ord('Q'):
            break

        elif c == 261:  # right
            cx = (cx + 1) % 8

        elif c == 258:  # down
            cy = (cy + 1) % len(cell)

        elif c == 259:  # up
            cy = (cy - 1) % len(cell)

        elif c == 260:  # left
            cx = (cx - 1) % 8

        elif c == 338 or c == ord('>'):  # pgdn
            font.iloc[currentchar] = list(cell)
            currentchar = (currentchar + 1) % 256
            cell = list(font.iloc[currentchar])

        elif c == 339 or c == ord('<'):  # pgup
            font.iloc[currentchar] = list(cell)
            currentchar = (currentchar - 1) % 256
            cell = list(font.iloc[currentchar])

        elif c == ord(' '):
            v = cell[cy]
            v = v ^ (0x80 >> cx)
            cell[cy] = v

        elif c == ord('i'):
            font.iloc[currentchar] = list(cell)
            if markchar is None:
                markchar = currentchar
            if markchar < currentchar:
                b = markchar
                e = currentchar
            else:
                b = currentchar
                e = markchar
            for i in range(b, e + 1):
                for r in range(0, len(cell)):
                    font.iloc[i][r] = font.iloc[i][r] ^ 0xff
            cell = list(font.iloc[currentchar])
            status("Inverted %d characters" % (e + 1 - b))
            markchar = None

        elif c == ord('I'):
            invert()
            status("Characters 128 to 256 inverted")

        elif c == ord('z'):
            for i in range(0, len(cell)):
                cell[i] = 0

        elif c == ord('m'):
            markchar = currentchar
            status("Mark placed")

        elif c == ord('c'):
            font.iloc[currentchar] = list(cell)
            clip = []
            if markchar is None:
                markchar = currentchar
            if markchar < currentchar:
                b = markchar
                e = currentchar
            else:
                b = currentchar
                e = markchar
            for i in range(b, e + 1):
                clip.append(list(font.iloc[i]))
            status("Copied %d characters" % len(clip))
            markchar = None

        elif c == ord('C'):
            relocate()
            status("Characters copied according to list")

        elif c == ord('p'):
            if clip is not None:
                b = currentchar
                for e in clip:
                    font.iloc[b] = e
                    b = (b + 1) % 256
                cell = list(clip[0])
                status("Pasted %d characters" % len(clip))
            else:
                status("Nothing to paste")

        elif c == ord('R'):
            cell.append(cell.pop(0))

        elif c == ord('r'):
            cell.insert(0, cell.pop())

        elif c == ord('T'):
            if len(cell) < 20:
                setfontheight(len(cell) + 1, 0)

        elif c == ord('S'):
            if len(cell) > 1:
                setfontheight(len(cell) - 1, 0)

        elif c == ord('t'):
            if len(cell) < 20:
                setfontheight(len(cell) + 1, 1)

        elif c == ord('s'):
            if len(cell) > 1:
                setfontheight(len(cell) - 1, 1)

        elif c == ord('E'):
            export()
            status("Font exported")

        elif c == ord('X'):
            export_scanlines()
            status("Scanlines exported")

        else:
            status("Unhandled key %s" % (str(c)))
    return


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Font editor')
    parser.add_argument('fontfile', metavar='FONTFILE',
                        help='Path to raw fontfile to edit')
    parser.add_argument('-c', '--convert', action='store_true',
                        help='Export scanlines')
    args = parser.parse_args()

    load(args.fontfile)

    if args.convert:
        relocate()
        invert()
        export_scanlines()
        print(f"Font {args.fontfile} was coverted to scanlines.fnt.c")
    else:
        curses.wrapper(editor)
