## fontedit.py

usage: fontedit.py [-h] [-c] FONTFILE

Added functionality:
 * Create inverse font: Capital I copies the lower 128 chars to the upper half
 and inverts each byte of font data by XORing it with 0xFF.
 * Increase/decrease character height: T / S and t / s adds and removes empty
 rows to and from the bottom or top of the character set.
 * VT100 char set: Captial C copies the characters from a code page 437 font set
 to their expected locations for VT100 terminal emulation.
 [(see file relocate.txt)](./relocate.txt)\
 Characters unavailable in CP437 fonts are predefined as either 8 or 16 pixel
 in height and will be copied accordingly.
 * Save as C code: Capital X exports the character set as C code data sorted by
 256 byte blocks for each row across all of the 256 characters from top to
 bottom. This is to conform with Luckynate4's changes to the render loop.\
 (Old export via Capital E instead exports all rows from each character before
 moving to the next character.)

When invoked with the -c flag, the font set is batch processed by first copying
the characters (like pressing C), then copying and inverting the top 128 chars
(like pressing I) and then exporting the C code (like pressing X) to
file scanlines.fnt.c. Fonts from the
[int10h font repo](https://github.com/viler-int10h/vga-text-mode-fonts) can be
converted without manual intervention for use in the firmware this way.