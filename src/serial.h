#ifndef _SERIAL_H_
#define _SERIAL_H_

#include <stdio.h>
#include <stdint.h>

#include "config.h"

#include "stm32f0xx.h"
#include "system_stm32f0xx.h"
#include "fifo.h"
#include "vgafont.h"
#include "ansi.h"

// List of baud rates that we can select from the menu
extern const int BAUD_RATES[]; 
#define B_115200 0
#define  B_57600 1
#define  B_38400 2
#define  B_19200 3
#define   B_9600 4
#define   B_4800 5
#define   B_2400 6
#define   B_1200 7

// Constants defining data settings we can choose from
#define U_7E1 0
#define U_7O1 1
#define U_7E2 2
#define U_7O2 3
#define U_8N1 4
#define U_8E1 5
#define U_8O1 6
#define PARITY_NONE 0
#define PARITY_EVEN 1
#define PARITY_ODD  2

#define ERROR_SYMBOL FONT_CHAR(1)

//#define AUTOBAUD

/* Buffer in 2^n */
#define RX_FIFO_Size 256
#define TX_FIFO_Size 16
#define USART_IRQ_PRIORITY 3

void USART_Init(void);
void Putchar(uint8_t data);
void PutStr(const char *string);
void Putint(uint8_t n);
void Puthex(uint8_t n);
void USART_Set_Baud(uint8_t baud_index); 
uint8_t USART_Get_Baud(void); 
uint8_t USART_Set_Params(uint8_t NewParams);
uint8_t USART_Get_Params(void);
void USART_Restart(void);
void USART_Pause(void);


extern FIFO_Data_t RxBuf[], TxBuf[];
#endif
