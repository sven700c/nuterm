/*
 * config.h
 *
 *  Author: ln4
 *
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.

    If not, see http://www.gnu.org/licenses/gpl-3.0.en.html
 */

#ifndef _CONFIG_H
#define _CONFIG_H

#define NUTERM_VERSION  "0.3"

/* user configuration follows */

/*
 * Set this if you want to see a startup message that
 * shows the current baud rate + communication
 * parameters
 */
#define START_MSG

/*
 * Enable this if you want to be able to use a menu to 
 * change the baud rate and comm parameters on the fly.
 * Press CTRL-Break to access the menu.
 */
#define USE_MENU_MODE

/* 
 * Configure these values as desired.
 * Jumper J1 can be used to choose from 3 different
 * baud rates that will be applied at power on/reset.
 * You can also use menu mode to change the baud rate. 
 */
#define DEFAULT_BAUD_A B_115200 // J1 open
#define DEFAULT_BAUD_B B_57600  // J1 pin 2+3 shorted
#define DEFAULT_BAUD_C B_9600   // J1 pin 1+2 shorted

/* 
 * These are the default communication parameters that
 * will be used at power on.  You can change them in
 * the menu mode if it is enabled.
 * See serial.h for the list of available comm parameters.
 */
#define DEFAULT_COM_PARAM U_8N1

/* 
 * Choose between two modes for Backspace.
 * define DEFAULT_BS_CTRL_QMARK for ^?.
 * Otherwise BS sends ^H by default.
 * Can change modes in Menu Mode.
 */
// #define DEFAULT_BS_CTRL_QMARK 

#endif
