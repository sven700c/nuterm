#ifndef _VGAFONT_H_ 
#define _VGAFONT_H_

// Displayed font size
// the number of screen lines, and so the amount of screen buffer memory required is 480/FONT_Y*80
// and for original 12 line font it results in 3200 bytes needed for just screen buffer. This does not leave
// enough RAM for ansi terminal emulation and other needs. 16 row font gives 30 lines of text, and it
// looks much better anyway.
//
#define FONT_X      8
#define FONT_Y      10

// Option to have 2 compiled in font, uncomment to enable
#define DUALFONT

#ifdef DUALFONT
extern const uint8_t *FONT_TABLE;
extern const uint8_t FONT_TABLE_PRI[ (FONT_Y * 256) ];
extern const uint8_t FONT_TABLE_ALT[ (FONT_Y * 256) ];

void Toggle_Font(void);
#else

extern const uint8_t FONT_TABLE[ (FONT_Y * 256) ];
#endif

// Padded size
#define FONT_ROW    10
#define FONT_COL    8

#define FONT_START  0
#define FONT_END    255 

#endif
