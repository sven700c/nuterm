/*
 * menu.c
 *
 * Created: 1-Aug-21
 *  Author: ln4@posteo.net
 * Copyright (c) 2021 by luckynate4
 *  - Provides menu for switching baud rates, comm settings,
 *    and backspace settings.
 *
 * 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.

    If not, see http://www.gnu.org/licenses/gpl-3.0.en.html
 */

#include <string.h>
#include <ctype.h>
#include "vga-min.h"
#include "vgafont.h"
#include "ansi.h"
#include "serial.h"
#include "ps2.h"
#include "menu.h"

static uint8_t MENU_BAUD=0;
static uint8_t MENU_COM_PARAMS=0;

/*
 * Display a text based menu, and highlight the currently set
 * baud rate, data settings, etc.
 */
void Menu_Display(void)
{
    ANSI_Init();
    ANSI_PutStr("nuterm (v");
    ANSI_PutStr(NUTERM_VERSION);
#ifdef DUALFONT
    ANSI_PutStr(")\n\r\n" \
                "Set baud rate\t\tData bits - Parity - Stop bits:\n\r\n" \
                "A = 115200\t\tI = 7-E-1\r\n" \
                "B =  57600\t\tJ = 7-O-1\r\n" \
                "C =  38400\t\tK = 7-E-2\r\n" \
                "D =  19200\t\tL = 7-O-2\r\n" \
                "E =   9600\t\tM = 8-N-1\r\n" \
                "F =   4800\t\tN = 8-E-1\r\n" \
                "G =   2400\t\tO = 8-O-1\r\n" \
                "H =   1200\n\r\n" \
                "Backspace sends:\tToggle Font:\n\r\n" \
                "P = ^H\t\t\tR = Pri / Alt\r\n" \
                "Q = ^?\n\r\n\n" \
                "Hit <Enter> to exit\r\n");
#else
    ANSI_PutStr(")\n\r\n" \
                "Set baud rate\t\tData bits - Parity - Stop bits:\n\r\n" \
                "A = 115200\t\tI = 7-E-1\r\n" \
                "B =  57600\t\tJ = 7-O-1\r\n" \
                "C =  38400\t\tK = 7-E-2\r\n" \
                "D =  19200\t\tL = 7-O-2\r\n" \
                "E =   9600\t\tM = 8-N-1\r\n" \
                "F =   4800\t\tN = 8-E-1\r\n" \
                "G =   2400\t\tO = 8-O-1\r\n" \
                "H =   1200\n\r\n" \
                "Backspace sends:\n\r\n" \
                "P = ^H\r\n" \
                "Q = ^?\n\r\n\n" \
                "Hit <Enter> to exit\r\n");
#endif
    ANSICursor_MoveTo(1,21);

    switch(MENU_BAUD) {
        case B_115200: HighlightChars(0,4,10);
                        break;
        case B_57600: HighlightChars(0,5,10);
                        break;
        case B_38400: HighlightChars(0,6,10);
                        break;
        case B_19200: HighlightChars(0,7,10);
                        break;
        case B_9600: HighlightChars(0,8,10);
                        break;
        case B_4800: HighlightChars(0,9,10);
                        break;
        case B_2400: HighlightChars(0,10,10);
                        break;
        case B_1200: HighlightChars(0,11,10);
                        break;
    }
    switch(MENU_COM_PARAMS) {
        case U_7E1: HighlightChars(24, 4, 9);
                                break;
        case U_7O1: HighlightChars(24, 5, 9);
                                break;
        case U_7E2: HighlightChars(24, 6, 9);
                                break;
        case U_7O2: HighlightChars(24, 7, 9);
                                break;
        case U_8N1: HighlightChars(24, 8, 9);
                                break;
        case U_8E1: HighlightChars(24, 9, 9);
                                break;
        case U_8O1: HighlightChars(24, 10, 9);
                                break;
    }
    if(Is_Backspace_CtrlH())
    {
        HighlightChars(0,15,6);
    } else {
        HighlightChars(0,16,6);
    }
#ifdef DUALFONT
    if(FONT_TABLE == FONT_TABLE_PRI)
    {
        HighlightChars(28,15,3);
    } else {
        HighlightChars(34,15,3);
    }
#endif
}

/*
 * Retrieve current settings so they can be highlighted in the menu.
 * Pause the USART so we don't receive or transmit anything until we
 * quit the menu.
 */
void Menu_Mode(void)
{
    MENU_BAUD = USART_Get_Baud();
    MENU_COM_PARAMS = USART_Get_Params();
    USART_Pause();
    Menu_Display();
}

/*
 * Adjust communication settings as required, restart
 * the USART, and quit menu mode.
 */
void Normal_Mode(void)
{

    ANSI_Init();

    USART_Set_Baud(MENU_BAUD);
    switch(MENU_COM_PARAMS) {
        case U_7E1: USART_Set_Params(U_7E1);
            break;
        case U_7O1: USART_Set_Params(U_7O1);
            break;
        case U_7E2: USART_Set_Params(U_7E2);
            break;
        case U_7O2: USART_Set_Params(U_7O2);
            break;
        case U_8N1: USART_Set_Params(U_8N1);
            break;
        case U_8E1: USART_Set_Params(U_8E1);
            break;
        case U_8O1: USART_Set_Params(U_8O1);
            break;
    }
    USART_Restart();
}

int Menu_Mode_Key(uint8_t c)
{
    if (!c)
        return 1;

    switch(ctoupper(c)) {
        case 13: // Enter to quit menu
            Normal_Mode();
            return 0;
        case 'A': // 115200
            MENU_BAUD = B_115200;
            break;
        case 'B': // 57600
            MENU_BAUD = B_57600;
          break;
        case 'C': // 38400
            MENU_BAUD = B_38400;
            break;
        case 'D': // 19200
            MENU_BAUD = B_19200;
            break;
        case 'E': // 9600
            MENU_BAUD = B_9600;
            break;
        case 'F': // 4800
            MENU_BAUD = B_4800;
            break;
        case 'G': // 2400
            MENU_BAUD = B_2400;
            break;
        case 'H': // 1200
            MENU_BAUD = B_1200;
            break;
        case 'I': // 7E1
            MENU_COM_PARAMS = U_7E1;
            break;
        case 'J': // 7O1
            MENU_COM_PARAMS = U_7O1;
            break;          
        case 'K': // 7E2
            MENU_COM_PARAMS = U_7E2;
            break;          
        case 'L': // 7O2
            MENU_COM_PARAMS = U_7O2;
            break;          
        case 'M': // 8N1
            MENU_COM_PARAMS = U_8N1;
            break;
        case 'N': // 8E1
            MENU_COM_PARAMS = U_8E1;
            break;
        case 'O': // 8O1
            MENU_COM_PARAMS = U_8O1;
            break;
        case 'P': // ^H
            Set_Backspace_CtrlH();
            break;
        case 'Q': // ^?
            Set_Backspace_CtrlQmark();
            break;
#ifdef DUALFONT
        case 'R': // Toggle Font
            Toggle_Font();
            break;
#endif
        default:
            break;

    }
    Menu_Display();
    return 1;
}
