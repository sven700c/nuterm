/*
 * vgafont.c
 *
 *  Author: Madis Kaal <mast@nomad.ee>
 *  Copyright (c) 2017 by Madis Kaal
 *
 *  Modified by: luckynate4 <ln4@posteo.net>
 *  Copyright (c) 2021
 *      - changed font and organized table by scanline
 
        This program is free software: you can redistribute it and/or modify
        it under the terms of the GNU General Public License as published by
        the Free Software Foundation, either version 3 of the License, or
        (at your option) any later version.

        This program is distributed in the hope that it will be useful,
        but WITHOUT ANY WARRANTY; without even the implied warranty of
        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
        GNU General Public License for more details.

        You should have received a copy of the GNU General Public License
        along with this program.

        If not, see http://www.gnu.org/licenses/gpl-3.0.en.html
 */

#include <stdint.h>
#include "vgafont.h"


/*  Font was converted from IBM VGA 8x16 font from: 
     Ultimate Oldschool PC fontpack by VileR
     https://int10h.org/oldschool-pc-fonts

    The 128 font glyph bitmaps are stored by scanline in the
    following table.  
    ie. array element 0-127: top scanline (row) of all glyphs
        array element 128-256: 2nd scanline of all glyphs
        etc...
*/
#ifdef DUALFONT
const uint8_t FONT_TABLE_PRI[ (FONT_Y * 256) ] = 
{
    #include "scanlines.fnt.c"
};

const uint8_t FONT_TABLE_ALT[ (FONT_Y * 256) ] = 
{
    #include "scanlines.fnt.alt.c"
};

const uint8_t *FONT_TABLE = FONT_TABLE_PRI;

void Toggle_Font()
{
    if (FONT_TABLE == FONT_TABLE_PRI)
        FONT_TABLE = FONT_TABLE_ALT;
    else
        FONT_TABLE = FONT_TABLE_PRI;
};
#else

const uint8_t FONT_TABLE[ (FONT_Y * 256) ] = 
{
    #include "scanlines.fnt.c"
};
#endif