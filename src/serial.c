/*
 * serial.c
 *
 * Created: March-09-16, 8:48:47 PM
 *  Author: K. C. Lee
 * Copyright (c) 2015 by K. C. Lee
 *
 * Copyright (c) 2021 by luckynate4 <ln4@posteo.net>
 *  - Added support functions for menu selectable baud rates and serial data settings.
 *  - Added support for primitive parity handling
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.

    If not, see http://www.gnu.org/licenses/gpl-3.0.en.html
    
 */

#include "config.h"

#include "serial.h"
#include "vga-min.h"
#include "fifo.h"
#include "ansi.h"

const int BAUD_RATES[] = { 
    115200,
    57600, 
    38400,
    19200,
    9600,
    4800,
    2400,
    1200 };
static uint8_t CUR_BAUD;
static uint8_t CUR_PARITY;
static uint8_t CUR_COM_PARAMS;

FIFO_DECL(RxBuf,RX_FIFO_Size);
FIFO_DECL(TxBuf,TX_FIFO_Size);

void USART_Init(void)
{
    RCC->APB2ENR |= RCC_APB2ENR_USART1EN;                   // Enable UART1 clock

    // uTerm-S: check PA2 (input) and drive PA3 (output) to select 
    // the baud rate and set it
    GPIOA->BRR = (1<<PA3);          // Set PA3 = 0
    if(GPIOA->IDR & GPIO_IDR_2)
    {
        // PA2 = 1 -> DEFAULT_BAUD_A (from config.h)
        USART_Set_Baud(DEFAULT_BAUD_A);
    }
    else
    {
        GPIOA->BSRR = (1<<PA3);     // Set PA3 = 1
        if(GPIOA->IDR & GPIO_IDR_2)
        {
            // PA2 = 1 -> DEFAULT_BAUD_B (from config.h)
            USART_Set_Baud(DEFAULT_BAUD_B);
        }
        else // PA2 = 0 -> DEFAULT_BAUD_C (from config.h)
            USART_Set_Baud(DEFAULT_BAUD_C);
    }
    
#ifdef AUTOBAUD
    USART1->CR2 = USART_CR2_ABRMODE_0|USART_CR2_ABREN;      // enable autobaud, 1 stop bit
#endif
    USART1->CR1 = USART_CR1_RXNEIE|                         // Receive not empty
                  USART_CR1_RE|USART_CR1_TE|USART_CR1_UE;   // 8-bit, 1 start bit, no parity, 16X oversampling

    USART_Set_Params(DEFAULT_COM_PARAM);                    // DEFAULT_COM_PARAM is defined in config.h

    FIFO_Clear((FIFO*)RxBuf);
    FIFO_Clear((FIFO*)TxBuf);
    
    // NVIC IRQ
    NVIC_SetPriority(USART1_IRQn,USART_IRQ_PRIORITY);       // Lowest priority// Highest priority
    NVIC_EnableIRQ(USART1_IRQn);
}

/* This function will pause USART reception by disabling transmit and receive interrupts,
 * and clearing the FIFOs.
 * This is useful when entering menu mode, and we don't want to receive any serial rx/tx.
 */
void USART_Pause(void)
{
    USART1->CR1 &= ~(USART_CR1_RXNEIE|USART_CR1_RE|USART_CR1_TE|USART_CR1_UE);

    FIFO_Clear((FIFO*)RxBuf);
    FIFO_Clear((FIFO*)TxBuf);
}

// Update the baud rate from one of the rates listed in BAUD_RATES[]
void USART_Set_Baud(uint8_t baud_index)
{
    USART1->BRR = CPU_CLOCK/BAUD_RATES[baud_index];
    CUR_BAUD = baud_index;
}

// return the current baud rate setting
uint8_t USART_Get_Baud(void)
{
    return CUR_BAUD;
}

// Restart USART tx/rx after it has been paused.
void USART_Restart(void)
{
    USART1->CR1 |= USART_CR1_RXNEIE|USART_CR1_RE|
                   USART_CR1_TE|USART_CR1_UE;
}


/* Set the serial data settings. */
uint8_t USART_Set_Params(uint8_t NewParams)
{
    uint8_t DataWordBits, // data bits + parity bits
            Parity,
            StopBits;

    USART1->CR1 &= ~(USART_CR1_RXNEIE|USART_CR1_RE|USART_CR1_TE|USART_CR1_UE); // disable the USART1 interrupts
    
    CUR_COM_PARAMS = NewParams;

    switch(NewParams) {
        case U_7E1:
            Parity = PARITY_EVEN;
            DataWordBits = 8;
            StopBits = 1;
            break;
        case U_7O1:
            Parity = PARITY_ODD;
            DataWordBits = 8;
            StopBits = 1;
            break;
        case U_7E2:
            Parity = PARITY_EVEN;
            DataWordBits = 8;
            StopBits = 2;
            break;
        case U_7O2:
            Parity = PARITY_ODD;
            DataWordBits = 8;
            StopBits = 2;
            break;
        case U_8N1: 
            Parity = PARITY_NONE;
            DataWordBits = 8;
            StopBits = 1;
            break;
        case U_8E1:
            Parity = PARITY_EVEN;
            DataWordBits = 9;
            StopBits = 1;
            break;
        case U_8O1:
            Parity = PARITY_ODD;
            DataWordBits = 9;
            StopBits = 1;
            break;
        default:
            return 0;
    }

    switch(DataWordBits) {
        case 8: USART1->CR1 &= ~(USART_CR1_M);
                        break;
        case 9: USART1->CR1 |= USART_CR1_M;
                        break;
        default: 
            return 0;
    }
    switch(Parity) {
        case PARITY_NONE: USART1->CR1 &= ~(USART_CR1_PCE);
                          CUR_PARITY = PARITY_NONE;
                          break;
        case PARITY_EVEN: USART1->CR1 |= USART_CR1_PCE;
                          USART1->CR1 &= ~(USART_CR1_PS);
                          CUR_PARITY = PARITY_EVEN;
                          break;
        case PARITY_ODD:  USART1->CR1 |= USART_CR1_PCE;
                          USART1->CR1 |= USART_CR1_PS;
                          CUR_PARITY = PARITY_ODD;
                          break;
        default:
            return 0;
    }

    switch(StopBits)
    {
        case 1: USART1->CR2 &= ~(USART_CR2_STOP_1|USART_CR2_STOP_0);
                        break;
        case 2: USART1->CR2 &= ~(USART_CR2_STOP_0);
                USART1->CR2 |= USART_CR2_STOP_1;
                        break;
    }

    USART_Restart();
    return 1;
}

// return the current serial data settings
uint8_t USART_Get_Params(void)
{
    return CUR_COM_PARAMS;
}


void USART1_IRQHandler(void)
{
    // if parity error or overrun we ignore it and print an error symbol.
    if(USART1->ISR & (USART_ISR_PE | USART_ISR_ORE | USART_ISR_NE | USART_ISR_FE))
    {
        USART1->RDR;
        USART1->RQR |= USART_RQR_RXFRQ;
        USART1->ICR |= (USART_ICR_PECF | USART_ICR_ORECF | USART_ICR_NCF | USART_ICR_FECF);

        VGA_Putch(ERROR_SYMBOL);
    }
    else if(USART1->ISR & USART_ISR_RXNE)                   // Rx data
    {
        FIFO_Write((FIFO*)RxBuf,USART1->RDR&~(0x80));       // clear parity bit. 
    }
    
    if(USART1->ISR & USART_ISR_TXE)                         // Tx empty
    {
        uint8_t TxD;
        
        if(FIFO_Read((FIFO*)TxBuf,&TxD))
            USART1->TDR = TxD;                              // TDR is 16-bit!
        else
            USART1->CR1 &= ~USART_CR1_TXEIE;                // Disable Tx Empty interrupt
    }
}

void Putchar( uint8_t data )
{
    while(!FIFO_Write((FIFO*)TxBuf,data));                  // busy wait 
    USART1->CR1 |= USART_CR1_TXEIE;                         // Enable Tx Empty interrupt
}


void PutStr(const char *string)
{
    while(string && *string)
        Putchar(*string++);
}

void Putint(uint8_t n)
{
    if(n>9)
        Putint(n/10);
    Putchar((n%10)+'0');
}

void Puthex(uint8_t n)
{
    uint8_t c;
    c=(n>>4)+'0';
    if (c>'9')
        c+=7;
    Putchar(c);
    c=(n&0x0f)+'0';
    if (c>'9')
        c+=7;
    Putchar(c);
}
