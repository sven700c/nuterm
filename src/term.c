/*
 * term.c
 *
 * Created: March-14-16, 5:18:46 PM
 *  Author: K. C. Lee
 * Copyright (c) 2016 by K. C. Lee 
 *
 * Copyright (c) 2021 by luckynate4 <ln4@posteo.net>
 *  - Added startup message showing menu hotkey
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.

    If not, see http://www.gnu.org/licenses/gpl-3.0.en.html
 */
 
#include "vga-min.h"
#include "ansi.h"
#include "serial.h"
#include "ps2.h"

int main(void)
{
    VGA_Init();
    USART_Init();
    PS2_Init();
    ANSI_Init();

#ifdef START_MSG
    VGA_PutStr("[nuterm ");
    switch(USART_Get_Baud()) {
        case B_115200: VGA_PutStr("115200");
            break;
        case B_57600: VGA_PutStr("57600");
            break;
        case B_38400: VGA_PutStr("38400");
            break;
        case B_19200: VGA_PutStr("19200");
            break;
        case B_9600: VGA_PutStr("9600");
            break;
        case B_4800: VGA_PutStr("4800");
            break;
        case B_2400: VGA_PutStr("2400");
            break;
        case B_1200: VGA_PutStr("1200");
            break;
    }
    switch(USART_Get_Params()) {
        case U_7E1: VGA_PutStr(" 7E1");
            break;
        case U_7O1: VGA_PutStr(" 7O1");
            break;
        case U_7E2: VGA_PutStr(" 7E2");
            break;
        case U_7O2: VGA_PutStr(" 7O2");
            break;
        case U_8N1: VGA_PutStr(" 8N1");
            break;
        case U_8E1: VGA_PutStr(" 8E1");
            break;
        case U_8O1: VGA_PutStr(" 8O1");
            break;
    }
#ifdef USE_MENU_MODE
    ANSI_PutStr(" ^BREAK for menu]\r\n");
#else
    ANSI_PutStr("]\r\n");
#endif
#endif

    while(1)
    {
        if(FIFO_ReadAvail((FIFO*)RxBuf))
            ANSI_FSM(Getc((FIFO*)RxBuf));

        if(FIFO_ReadAvail((FIFO*)PS2_Buf))
            PS2_Task();
        
        if(Cursor.Update)
            Cursor_Task();

    }
}
