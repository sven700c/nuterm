/*
 * 15khz-min.h
 *
 * Created: June-16 2023
 *  Author: S. Simon
 * Copyright (c) 2023 S. Simon
 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.

    If not, see http://www.gnu.org/licenses/gpl-3.0.en.html
    
 */ 

// Select 15Khz mode
//#define NTSC
#define PAL

#ifdef NTSC
#define VGA_DOTCLK              14285714UL  //Hz
#define CFGR_PLL_MULT           RCC_CFGR_PLLMUL8
#define CFGR2_PLL_PREDIV        RCC_CFGR2_PREDIV1_DIV7

// NTSC timings with 14.2857Mhz pixel clock
#define VGA_PIX_X               908  // ~63.5us or 15.733kHz H freq
#define VGA_BORDER_LEFT         55
#define VGA_HSYNC_FP            21
#define VGA_HSYNC               67
#define VGA_HSYNC_BP            67
#define VGA_BORDER_RIGHT        58

#define VGA_LINES               262  // 60.05Hz V freq
#define VGA_RES_Y               216  // 80x24 with 9 pixel font
#define VGA_BORDER_TOP          5
#define VGA_VSYNC_FP            11
#define VGA_VSYNC               9
#define VGA_VSYNC_BP            16
#define VGA_BORDER_BOT          5
#define VGA_VSYNC_NEG
#else


#ifdef PAL
#define VGA_DOTCLK              14062500UL  //Hz
#define CFGR_PLL_MULT           RCC_CFGR_PLLMUL9
#define CFGR2_PLL_PREDIV        RCC_CFGR2_PREDIV1_DIV8

// PAL timings with 14.0625Mhz pixel clock
#define VGA_PIX_X               900  // 64us or 15.625kHz H freq
#define VGA_BORDER_LEFT         50
#define VGA_HSYNC_FP            24
#define VGA_HSYNC               66
#define VGA_HSYNC_BP            80
#define VGA_BORDER_RIGHT        40

#define VGA_LINES               312   // 50.09 Hz V freq
#define VGA_RES_Y               240   // 80x24 char with 10px font
#define VGA_BORDER_TOP          20
#define VGA_VSYNC_FP            10
#define VGA_VSYNC               9
#define VGA_VSYNC_BP            21
#define VGA_BORDER_BOT          21
#define VGA_VSYNC_NEG


#endif
#endif


// Other timings I've been tinkering with:

// Working with 12.5Mhz pixel clock
//#define VGA_PIX_X             794
//#define VGA_BORDER_LEFT       8
//#define VGA_HSYNC_FP          8
//#define VGA_HSYNC             88
//#define VGA_HSYNC_BP          42
//#define VGA_BORDER_RIGHT      8

// PAL timings with 14.286Mhz pixel clock
//#define VGA_PIX_X               914   // ~64μs or 15.629kHz H freq
//#define VGA_BORDER_LEFT         48
//#define VGA_HSYNC_FP            24    // 1.65 +0.4 −0.1μs
//#define VGA_HSYNC               67    // 4.7 ±0.20μs
//#define VGA_HSYNC_BP            82    // 5.7 ±0.20μs
//#define VGA_BORDER_RIGHT        53

// #ifdef CGA_200
// #define VGA_RES_Y              200
// #define VGA_BORDER_TOP         5
// #define VGA_VSYNC_FP           10
// #define VGA_VSYNC              8
// #define VGA_VSYNC_BP           30
// #define VGA_BORDER_BOT         8
// #define VGA_LINES              261