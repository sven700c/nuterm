Theory of Operation
===================

### Timers

The following timers are available:
- `TIM1`: Advanced timer (§13 p.221)
- `TIM3`: General purpose timer (§14 p.296)
  - §14.4 (p.338) lists the registers
- `TIM6/TIM7`: Basic timer (§15 p.361)
- `TIM14`: General purpose timer (§16 p.374)
- `TIM15/16/17`: General purpose timer (§17 p.397)

The TIM3 registers (all prefixed by `TIM3_` are:

    Reg Bits    Descr
    CR1 15:10   reserved
         9:8    CKD[1:0] clock div 00=0 01=2 10=4 11=reserved
         7      ARPE Auto-reload preload from ARR 0=not buffered 1=buffered
         6:5    CMS  00=edge aligned (counts per DIR bit)
                     01,10,11=center-aligned modes count up/down alternately
         4      DIR 0=up 1=down
         3      OPM one-pulse mode: at update 0=continue 1=stop
         2      URS update req. source for UEV events interrupt/DMA generation
                    1=only counter over/underflow 0=also UG bit or slave mode
         1      UDIS update disable for UEV gen. 0=disabled 1=enabled
         0      CEN counter enable 0=disabled 1=enabled
    CR2 15:8    reserved
         7      TI1S TI1 sel 0=CH1 pin connected to TI1 input
                             1=CH1/2/3 pins XORed to T1 input
         6:4    MMS master mode selection
         3      CCDS capture/compare DMA selection
         2:0    reserved; always read as 0
    SMCR        slave mode control register
    DIER        DMA/Interrupt enable
    SR          status register
    EGR         event generation register
    CCMR1       capture/compare mode register 1; reset=0000
         _OCn   n=2,1 output compare mode    
         _ICn   n=2,1 input capture mode
    CCMR2       capture/compare mode register 2; reset=0000
         _OCn   n=4,3 output compare mode    
         _ICn   n=4,3 input capture mode
    CCER        capture/compare enable; reset=0000
                  bits differ if CCn channel is configured as input or output
        15:12   CC4*
        11:8    CC3*
         7:4    CC2*
         3      CC1NP output polarity
                  out: must be 0
                  in: defines polarity w/CC1P
         2      reserved
         1      CC1P polarity
                  out: OC1 active 0=high 1=low
                  in:  CC1NP and CC1P select:
                       00=noninverting, rising edge
                       01=inverted faling edge
                       10=reserved
                       11=noninverted, both edges
         0      CC1E capture/compare output enable
                  out: 0=off OC1 not active, 1=on OC1 → output pin
                  in: counter→CCR1 0=disabled 1=enabled
    
    CNT         counter; reset=00000000

    PSC         prescaler; reset=0000

    ARR         Auto-reload register; reset=FFFFFFFF

    CCR1        capture/compare register 1; reset=00000000
    CCR2        *
    CCR3        *
    CCR4        *

    DCR         DMA control register
        15:13   reserved, always read 0
        12:8    DBL[4:0] DMA burst length`
         7:5    reserved, always read 0
         4:0    DBA[4:0] DMA base address



### Video Timing

The video timing is generated by `TIM3`.


### References

- [STM32F0x0 Documentation][stm32f0x0-doc]. Note that this is a different
  documentation page from the STM32F0x[128] series.
- [RM0360 Reference manual: STM32F030x4/x6/x8/xC and STM32F070x6/xB advanced
  ARM® -based 32-bit MCUs][stm32f0x0-RM0360]

[stm32f0x0-RM0360]: https://www.st.com/resource/en/reference_manual/rm0360-stm32f030x4x6x8xc-and-stm32f070x6xb-advanced-armbased-32bit-mcus-stmicroelectronics.pdf
[stm32f0x0-doc]: https://www.st.com/en/microcontrollers-microprocessors/stm32f0x0-value-line.html#documentation

